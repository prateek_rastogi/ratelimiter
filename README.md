    Framework that can throttle client requests in distributed system.
	Throttling could be on multiple level i.e per client, per endpoint etc.
	In case client limit has breached the framework would reject the request with appropriate http status.


    Rate Limiter Library provides ability to Rate Limit based on following params:
    1. OverAll Rate limit Based on Client ID
    2. Rate limit based on HTTP Methods , Currently GET| POST is supported
    3. Rate Limit based Http End Points 

All Rate limits can be applied in seconds,minute,hour,day,week,month based on same order.

### What is this repository for? ###

* Framework Exposed as Jar

* This supports following Type of Rate Limiter:
   
 1. Distributed MemorySync Rate Limiter (Sliding Window): Code would run on Each Node and would register each Node in Mysql giving us total server count . Now Resultant
    threshold would be threshold/NumServers in Memeory of Each Node . Server Register API Ping , Health Check.	   
 2. SimpleRedis Limiter : All Counters would be persisted in Redis in Single Call via lUA script. 
* Version 0.0.1

### How do I get set up? ###

1.Mysql/DB would need to setup with script.txt 
   
   spring:
     redis:
     host:localhost
     port:6379
     
     spring.datasource.url: jdbc:mysql://localhost:3306/ninja
     spring.datasource.username: root
     spring.datasource.password: paytm@197

2.Pom Entry :

        <dependency>
			<groupId>com.ninja</groupId>
			<artifactId>RateLimit</artifactId>
			<version>0.0.1</version> 
        </dependency>

3.Register Listener 

		@Configuration
        public class AppConfig extends WebMvcConfigurerAdapter{

	     @Autowired
	     RateLimitInterceptor requestInterceptor;
	
	     @Override
	     public void addInterceptors(InterceptorRegistry registry) {
		    registry.addInterceptor(requestInterceptor);
	     }
       }
4.Interceptor (Already Done)

   @Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {

		String clientId = request.getHeader("client_id");
		String methodType = request.getMethod();
		String requestURI = request.getRequestURI();
		if(ObjectUtils.isEmpty(clientId)){
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "UnAuthorised!!");
			logger.info("Request Thottled for Client Id {}, Request Type");
			return false;
		}
		Boolean isOverlimit = rateLimitService.processLimitsCounters(clientId, methodType, requestURI);
		if (!isOverlimit) {
			response.sendError(429, "Too Many Requests");
			logger.info("Request Thottled for Client Id {}, Request Type");
			return false;
		} 
		return true;
	}
5.  StartUp Load
     RateLimiter service = context.getBean(MemorySycnRateLimiterService.class);
	     service.loadClientConfig();  
6.  Application.yml 

    

### Who do I talk to? ###
  finch986@gmail.com
