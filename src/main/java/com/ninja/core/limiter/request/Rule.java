package com.ninja.core.limiter.request;

/**
 * Defines a rule type supported in Framework.
 */
public class Rule {

	private RuleType type=RuleType.GLOBAL;
	private String ruleKey = "";
	
	public Rule(RuleType type,String ruleKey){
		this.ruleKey = ruleKey;
		this.type = type;
	}
	public Rule(){
	}
	public RuleType getType() {
		return type;
	}
	public void setType(RuleType type) {
		this.type = type;
	}
	public String getRuleKey() {
		return ruleKey;
	}
	public void setRuleKey(String ruleKey) {
		this.ruleKey = ruleKey;
	}
	@Override
    public String toString() {
        return type.toString()+"."+Constants.KEY_DELIMITER+ruleKey;
    }
	
}
