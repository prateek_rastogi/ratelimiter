package com.ninja.core.limiter.request;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.ninja.limiter.config.Limit;

/**
 * Defines a limit rule that can support regular and token bucket rate limits.
 */
public class RuleLimit {

    private final RuleTimeUnit timeUnit;
    private final Number limit;
    private final Number expiry;

    public static List<RuleLimit> getRuleLimits(Limit limit){
    	List<RuleLimit> ruleLimits = new ArrayList<RuleLimit>();
    	RuleLimit ruleLimit = null;
    	if(limit.getSEC() != null){
    		ruleLimit = new RuleLimit(RuleTimeUnit.SEC,limit.getSEC(), 1); 
    		ruleLimits.add(ruleLimit);
    	}
    	if(limit.getMIN() != null){
    		ruleLimit = new RuleLimit(RuleTimeUnit.MIN,limit.getMIN() , 60 ); 
    		ruleLimits.add(ruleLimit);
    	}
    	if(limit.getHOUR() != null){
    		ruleLimit = new RuleLimit(RuleTimeUnit.HOUR,limit.getHOUR(),  60 * 60 ); 
    		ruleLimits.add(ruleLimit);
    	}
    	if(limit.getDAY() != null){
    		ruleLimit = new RuleLimit(RuleTimeUnit.DAY,limit.getDAY() , 24 * 60 * 60 ); 
    		ruleLimits.add(ruleLimit);
    	}
    	if(limit.getWEEK() != null){
    		ruleLimit = new RuleLimit(RuleTimeUnit.WEEK,limit.getWEEK() , 7 * 24 * 60 * 60); 
    		ruleLimits.add(ruleLimit);
    	}
    	if(limit.getMONTH() != null){
    		ruleLimit = new RuleLimit(RuleTimeUnit.MONTH,limit.getMONTH() ,  30 * 24 * 60 * 60 ); 
    		ruleLimits.add(ruleLimit);
    	}    	
    	return ruleLimits;
    }
    public RuleLimit(RuleTimeUnit timeUnit, Number limit,Number expiry) {
        this.timeUnit = timeUnit;
        this.limit = limit;
        this.expiry = expiry;
    }
    /**
     * @return The limit.
     */
    public Number getLimit() {
        return limit;
    }

	public RuleTimeUnit getTimeUnit() {
		return timeUnit;
	}
	public Number getExpiry() {
		return expiry;
	}
	


    
}
