package com.ninja.core.limiter.request;

import java.util.Objects;

/**
 * Defines a limit rule that can support regular and token bucket rate limits.
 */
public class ThrottleRule {

	
    
	private Rule rule;
    private RuleLimit limit;
    private String cliendId;
    
	public Rule getRule() {
		return rule;
	}
	public void setRule(Rule rule) {
		this.rule = rule;
	}
	public RuleLimit getLimit() {
		return limit;
	}
	public void setLimit(RuleLimit limit) {
		this.limit = limit;
	}
    
	public String getCliendId() {
		return cliendId;
	}
	public void setCliendId(String cliendId) {
		this.cliendId = cliendId;
	}
	@Override
    public int hashCode() {
        return Objects.hash(rule,limit);
    }
	@Override
    public String toString() {
        return Constants.KEY_DELIMITER+rule;
    }
	
}
