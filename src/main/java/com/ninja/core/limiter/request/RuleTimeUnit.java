package com.ninja.core.limiter.request;

public enum RuleTimeUnit {
	SEC {
		public int getRank()   { return 0; }
		public String toString(){
			return "SECOND";
		}
	},
	MIN  {
		public int getRank()   { return 1; }
		public String toString(){
			return "MIN";
		}
	},
	HOUR {
		public int getRank()   { return 2; }
		public String toString(){
			return "HOUR";
		}
	},
	DAY {
		public int getRank()   { return 3; }
		public String toString(){
			return "DAY";
		}
	},
	WEEK{
		public int getRank()   { return 4; }
		public String toString(){
			return "WEEK";
		}
	},
	MONTH{
		public int getRank()   { return 5; }
		public String toString(){
			return "MONTH";
		}
	}
}
