package com.ninja.core.limiter.request;

public enum RuleType {
	GLOBAL {
		public int getRank()   { return 0; }
		public String toString(){
			return "GLOBAL";
		}
	},
	METHOD  {
		public int getRank()   { return 1; }
		public String toString(){
			return "METHOD";
		}
	},
	API {
		public int getRank()   { return 2; }
		public String toString(){
			return "API";
		}
	}
}
