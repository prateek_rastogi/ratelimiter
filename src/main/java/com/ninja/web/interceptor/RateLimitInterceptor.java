package com.ninja.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.ninja.limiter.service.RateLimiter;

@Component
public class RateLimitInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = LoggerFactory.getLogger(RateLimitInterceptor.class);

    @Qualifier("MemprySyncRateLimiter")
	@Autowired
	RateLimiter rateLimitService;
	
	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {

		String clientId = request.getHeader("client_id");
		String methodType = request.getMethod();
		String requestURI = request.getRequestURI();
		if(ObjectUtils.isEmpty(clientId)){
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "UnAuthorised!!");
			logger.info("Request Thottled for Client Id {}, Request Type");
			return false;
		}
		Boolean isOverlimit = rateLimitService.processLimitsCounters(clientId, methodType, requestURI);
		if (!isOverlimit) {
			response.sendError(429, "Too Many Requests");
			logger.info("Request Thottled for Client Id {}, Request Type");
			return false;
		} 
		return true;
	}
}
