package com.ninja.web.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class HealthController {
	
	/*curl -i -H "Accept:application/json" -H "Content-Type:application/json" -H "client_id:E-COM" http://127.0.0.1:8080/ping */
     
	@RequestMapping(value="/ping",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String getPing() throws Exception {
		return "Hi";
	}


}
