package com.ninja.convertor;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.util.ObjectUtils;

import com.ninja.core.limiter.request.Constants;
import com.ninja.core.limiter.request.Rule;
import com.ninja.core.limiter.request.RuleLimit;
import com.ninja.core.limiter.request.RuleType;
import com.ninja.core.limiter.request.ThrottleRule;
import com.ninja.limiter.config.ClientConfiguration;
import com.ninja.limiter.config.Limit;
import com.ninja.limiter.config.Specialization;
import com.ninja.limiter.config.SpecializationType;

public class ThrottleRuleConvertor {

	
	public static void getThrottleRuleFromConfiguration(ClientConfiguration configuration,Map<String,Set<ThrottleRule>> throttleRules,Map<String,Set<ThrottleRule>> clientApiMethodRules){
		if(!ObjectUtils.isEmpty(configuration.getLimit())){		    	
	    	 List<RuleLimit> ruleLimits = RuleLimit.getRuleLimits(configuration.getLimit());
	    	 for(RuleLimit ruleLimit : ruleLimits){
	    		 ThrottleRule throttleRule = new ThrottleRule();
		    	 throttleRule.setRule(new Rule());
		    	 throttleRule.setCliendId(configuration.getClient());
		    	 throttleRule.setLimit(ruleLimit);
		    	 
		    	 Set<ThrottleRule> clientThrottleRules= throttleRules.get(configuration.getClient());
		    	 if(null == clientThrottleRules){
		    		 clientThrottleRules = new HashSet<ThrottleRule>();
		    		 throttleRules.put(configuration.getClient(), clientThrottleRules);
		    	 }
		    	 clientThrottleRules.add(throttleRule);
		    	 processClientApiMethod(configuration.getClient(),throttleRule,clientApiMethodRules);
	    	 }
	     }
	  		     
	     List<Specialization> specializations = configuration.getSpecialization();
	     if(!ObjectUtils.isEmpty(specializations)){
	    	 for(Specialization specialization : specializations){
	    		 Map<String,Limit> limitMap = null;	
	    		 RuleType ruleType = null;
	    		 if(specialization.getType().equals(SpecializationType.METHOD)){
	    			 limitMap = specialization.getMethods();
	    			 ruleType = RuleType.METHOD;
	    		 }
	    		 if(specialization.getType().equals(SpecializationType.API)){
	    			 limitMap = specialization.getEndpoints();
	    			 ruleType = RuleType.API;
	    		 }
	    		 if(null == limitMap){
	    			 continue;
	    		 }
	    		 for(Map.Entry<String, Limit> mapEntry : limitMap.entrySet()){
	    			 List<RuleLimit> ruleLimits = RuleLimit.getRuleLimits(mapEntry.getValue());
	    			 for(RuleLimit ruleLimit : ruleLimits){
			    		 ThrottleRule throttleRule = new ThrottleRule();
				    	 throttleRule.setRule(new Rule(ruleType,mapEntry.getKey()));
				    	 throttleRule.setLimit(ruleLimit);
				    	 throttleRule.setCliendId(configuration.getClient());
				    	 
				    	 Set<ThrottleRule> clientThrottleRules= throttleRules.get(configuration.getClient());
				    	 if(null == clientThrottleRules){
				    		 clientThrottleRules = new HashSet<ThrottleRule>();
				    		 throttleRules.put(configuration.getClient(), clientThrottleRules);
				    	 }
				    	 clientThrottleRules.add(throttleRule);
				    	 processClientApiMethod(configuration.getClient(),throttleRule,clientApiMethodRules);
			    	 }
	    			 
	    		 }
	    		 
	    	 }
	    	 
	     }
	}
	public static void processClientApiMethod(String clientId,ThrottleRule rule,Map<String,Set<ThrottleRule>> rules){
		String key = clientId+Constants.KEY_DELIMITER+rule.getRule().getType()+Constants.KEY_DELIMITER+rule.getRule().getRuleKey();
		Set<ThrottleRule> throttleRules = rules.get(key);
	     if(throttleRules == null){
	    	 throttleRules = new HashSet<ThrottleRule>();
	    	 rules.put(key, throttleRules);
	     }
	     throttleRules.add(rule);
	}
}
