package com.ninja;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.ninja.limiter.service.RateLimiter;
import com.ninja.limiter.service.MemorySycnRateLimiterService;

@SpringBootApplication
@ComponentScan(basePackages = "com.ninja")
@EnableScheduling
@ComponentScan(basePackages = "com.ninja")
@EnableJpaRepositories(basePackages = "com.ninja")
@EntityScan(basePackages="com.ninja")
public class Application extends WebMvcConfigurerAdapter{  
	   
	
	public static void main(String[] args) {
	    ApplicationContext context = SpringApplication.run(Application.class, args);
	    RateLimiter service = context.getBean(MemorySycnRateLimiterService.class);
	    service.loadClientConfig();
	}
	
	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		return new JedisConnectionFactory();
	}

	@Bean
	RedisTemplate< String, Integer > redisTemplate() {
		final RedisTemplate< String, Integer > template =  new RedisTemplate< String, Integer >();
		template.setConnectionFactory( jedisConnectionFactory() );
		template.setKeySerializer( new StringRedisSerializer() );
		template.setHashValueSerializer( new GenericToStringSerializer< Integer >( Integer.class ) );
		template.setValueSerializer( new GenericToStringSerializer< Integer >( Integer.class ) );
		return template;
	}
}            
