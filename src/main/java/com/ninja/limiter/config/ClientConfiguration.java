package com.ninja.limiter.config;

import java.util.List;

public class ClientConfiguration {

	private String client;
	private Limit limit;
	private List<Specialization> specialization;
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public Limit getLimit() {
		return limit;
	}
	public void setLimit(Limit limit) {
		this.limit = limit;
	}
	public List<Specialization> getSpecialization() {
		return specialization;
	}
	public void setSpecialization(List<Specialization> specialization) {
		this.specialization = specialization;
	}



}
