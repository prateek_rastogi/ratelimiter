package com.ninja.limiter.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@ConfigurationProperties
public class ClientConfigurationLoader {

	private List<ClientConfiguration> clients;

	public List<ClientConfiguration> getClients() {
		return clients;
	}
	public void setClients(List<ClientConfiguration> clients) {
		this.clients = clients;
	}

}
