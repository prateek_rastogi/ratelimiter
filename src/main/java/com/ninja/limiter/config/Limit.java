package com.ninja.limiter.config;

public  class Limit {
	private Integer SEC;
	private Integer MIN;
	private Integer HOUR;
	private Integer DAY;	
	private Integer WEEK;
	private Integer MONTH;
	
	public Integer getSEC() {
		return SEC;
	}
	public void setSEC(Integer sEC) {
		SEC = sEC;
	}
	public Integer getMIN() {
		return MIN;
	}
	public void setMIN(Integer mIN) {
		MIN = mIN;
	}
	public Integer getHOUR() {
		return HOUR;
	}
	public void setHOUR(Integer hOUR) {
		HOUR = hOUR;
	}
	public Integer getDAY() {
		return DAY;
	}
	public void setDAY(Integer dAY) {
		DAY = dAY;
	}
	public Integer getWEEK() {
		return WEEK;
	}
	public void setWEEK(Integer wEEK) {
		WEEK = wEEK;
	}
	public Integer getMONTH() {
		return MONTH;
	}
	public void setMONTH(Integer mONTH) {
		MONTH = mONTH;
	}

}