package com.ninja.limiter.config;

import java.util.Map;

public class Specialization {
	private SpecializationType type;
	private Map<String,Limit> endpoints;	
	private Map<String,Limit> methods;
	public SpecializationType getType() {
		return type;
	}
	public void setType(SpecializationType type) {
		this.type = type;
	}
	public Map<String, Limit> getEndpoints() {
		return endpoints;
	}
	public void setEndpoints(Map<String, Limit> endpoints) {
		this.endpoints = endpoints;
	}
	public Map<String, Limit> getMethods() {
		return methods;
	}
	public void setMethods(Map<String, Limit> methods) {
		this.methods = methods;
	}	
}
