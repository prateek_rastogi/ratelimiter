package com.ninja.limiter.core;

public interface TTLCounter {

    /**
     * @param key - The key to associate with the cache
     * @param value - The actual value to store in the cache
     * @param timeToLiveSeconds - The time to live for this object in the cache
     */
    public Boolean incr(String key);

    public void createUpdateKey(String key , Integer threshold , Long timetoLive);

}