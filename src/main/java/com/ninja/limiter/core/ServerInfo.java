package com.ninja.limiter.core;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "server_info",catalog = "ninja")
public class ServerInfo {

	private Integer               id;

	private String                serverIdentifier;
	private Integer               priority;
	private Integer               priorityBucket;
	
	private Date                  created;
	private Date                  updated;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "server_identifier", nullable = false, length = 255 ,unique = true)
	public String getServerIdentifier() {
		return serverIdentifier;
	}
	public void setServerIdentifier(String serverIdentifier) {
		this.serverIdentifier = serverIdentifier;
	}
	@Column(name = "priority", nullable = false)
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	@Column(name = "priority_bucket", nullable = false)
	public int getPriorityBucket() {
		return priorityBucket;
	}
	public void setPriorityBucket(int priorityBucket) {
		this.priorityBucket = priorityBucket;
	}
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false, length = 19)
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated")
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	
}
