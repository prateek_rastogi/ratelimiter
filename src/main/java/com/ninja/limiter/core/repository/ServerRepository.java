package com.ninja.limiter.core.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ninja.limiter.core.ServerInfo;

public interface ServerRepository extends CrudRepository<ServerInfo,Long> {

	@Query("select count(*) from com.ninja.limiter.core.ServerInfo p where p.updated >= ? and p.updated < ?")	
    int fetchCountByDateRange(Date start, Date end);
    
	@Query("select s from com.ninja.limiter.core.ServerInfo s where s.serverIdentifier= ?")
    ServerInfo findByServerIdentifier(String identifier);
}