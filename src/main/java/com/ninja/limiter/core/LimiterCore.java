package com.ninja.limiter.core;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

@Component
public class LimiterCore implements TTLCounter{
	
	Map<String,AtomicLimit> limits = new ConcurrentHashMap<String,AtomicLimit>();
  
    @Override
	public Boolean incr(String key) {
    	Boolean isSuccessful = false;
	   	AtomicLimit limit = limits.get(key);
	   	if(ObjectUtils.isEmpty(limit)){
	   		limit = new AtomicLimit();
	   		limits.put(key, limit);	   		
	   	}
	   	isSuccessful  = limit.incr();
	   	return isSuccessful;
	}

	@Override
	public void createUpdateKey(String key, Integer threshold, Long timetoLive) {		
		AtomicLimit limit = limits.get(key);
		if(ObjectUtils.isEmpty(limit)){
			limit = new AtomicLimit(threshold,timetoLive);
			limits.put(key, limit);
		}
		if(limit.getTimeToLive() == timetoLive && limit.getThreshold().intValue() == threshold){
			return;
		}
		limit.setTimeToLive(timetoLive);
		limit.setThreshold(threshold);
		limit.reset();		
	}
	
}
