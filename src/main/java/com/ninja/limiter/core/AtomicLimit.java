package com.ninja.limiter.core;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class AtomicLimit {

	private AtomicInteger curValue = new AtomicInteger(0);
	private AtomicInteger threshold = new AtomicInteger(100);
	private AtomicLong    expiryInstant = new AtomicLong(-1L);
	private AtomicLong    startInstant = new AtomicLong(System.currentTimeMillis());
	private long timeToLive = 0;
	
	public AtomicLimit(){
   		this(0, 100, -1L);
	}
	public void reset(){
		curValue.set(0);
		long now = System.currentTimeMillis();
		expiryInstant.set(now + timeToLive * 1000);
		startInstant.set(now);
	}
	public AtomicLimit(Integer threshold,Long timeToLive){
   		this(0, threshold, timeToLive);
	}
	public AtomicLimit(Integer curValue, Integer threshold, Long timeToLive){
		long now = System.currentTimeMillis();
		this.startInstant.set(now);
		this.curValue.set(curValue);
		this.threshold.set(threshold);
		this.expiryInstant.set(now+timeToLive * 1000);
		this.timeToLive = timeToLive;
	}
	public Integer getCurValue() {
		return curValue.get();
	}
	public void setCurValue(Integer curValue) {
		this.curValue.set(curValue);
	}
	public Integer getThreshold() {
		return threshold.get();
	}
	public void setThreshold(Integer threshold) {
		this.threshold.set(threshold);
	}
	public Long getExpiryInstant() {
		return expiryInstant.get();
	}
	public void setExpiryInstant(Long expiryInstant) {
		this.expiryInstant.set(expiryInstant);
	}
	public long getTimeToLive() {
		return timeToLive;
	}
	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}
	synchronized public Boolean incr(){
		Boolean retVal = false;
		int current = curValue.get();
		long now = System.currentTimeMillis();
		if(now > expiryInstant.get()){
			reset();
		}
		if(threshold.get() < (current + 1)){
			return retVal;
		}
		curValue.incrementAndGet();
		retVal = true;
		return retVal;
	}

}
