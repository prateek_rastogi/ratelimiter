package com.ninja.limiter.service;

public interface RateLimiter {
	public void loadClientConfig();
	public boolean processLimitsCounters(String clientId,String requestType ,String endPoint);
}
