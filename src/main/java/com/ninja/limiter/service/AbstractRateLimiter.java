package com.ninja.limiter.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import com.ninja.convertor.ThrottleRuleConvertor;
import com.ninja.core.limiter.request.Constants;
import com.ninja.core.limiter.request.RuleType;
import com.ninja.core.limiter.request.ThrottleRule;
import com.ninja.limiter.config.ClientConfiguration;
import com.ninja.limiter.config.ClientConfigurationLoader;


public abstract class AbstractRateLimiter implements RateLimiter{

	private static final Logger logger = LoggerFactory.getLogger(AbstractRateLimiter.class);

	
	protected Map<String,Set<ThrottleRule>> throttleRules = new HashMap<String,Set<ThrottleRule>>();
	protected Map<String,Set<ThrottleRule>> clientMethodAPIToRule = new HashMap<String,Set<ThrottleRule>>();
	
			
	@Autowired
	private ClientConfigurationLoader configLoader;
    
	@Override
	public void loadClientConfig(){
		logger.info("Loading Client Configuration Rules..");
		List<ClientConfiguration> clientConfigs = configLoader.getClients();
		if(ObjectUtils.isEmpty(clientConfigs)){
			return;
		}
		for(ClientConfiguration configuration : clientConfigs){
		   ThrottleRuleConvertor.getThrottleRuleFromConfiguration(configuration, throttleRules,clientMethodAPIToRule);
		}
		logger.info("Client Configuration Loaded Successfully..");
	}
	/**
	 * 
	 *
	 * @param clientId
	 * @param requestType
	 * @param endPoint
	 * @return
	 * This method calculates counters and limits that need to be applied Request 
	 * and Applies Rate limiting Logic
	 */
	Set<ThrottleRule> fetchMatchingRules(String clientId,String requestType ,String endPoint){
		Set<ThrottleRule> rules = throttleRules.get(clientId);
		if(ObjectUtils.isEmpty(clientId) || ObjectUtils.isEmpty(requestType) || ObjectUtils.isEmpty(endPoint)){
        	logger.debug("Invalid Client Id or RequestType or endPoint for Throttling..");
        	return rules;
        }		
   		if(ObjectUtils.isEmpty(rules)){
        	logger.debug("No Throttling Rule Set Found for Client Id {}",clientId);
        	return rules;
        }
        rules.clear();
        if(clientMethodAPIToRule.get(clientId+Constants.KEY_DELIMITER+RuleType.GLOBAL+Constants.KEY_DELIMITER) != null){
        	rules.addAll(clientMethodAPIToRule.get(clientId+Constants.KEY_DELIMITER+RuleType.GLOBAL+Constants.KEY_DELIMITER));
        }
        if(clientMethodAPIToRule.get(clientId+Constants.KEY_DELIMITER+RuleType.METHOD+Constants.KEY_DELIMITER+requestType) != null){
        	rules.addAll(clientMethodAPIToRule.get(clientId+Constants.KEY_DELIMITER+RuleType.METHOD+Constants.KEY_DELIMITER+requestType));
        }
        if(clientMethodAPIToRule.get(clientId+Constants.KEY_DELIMITER+RuleType.API+Constants.KEY_DELIMITER+endPoint) !=null){
        	rules.addAll(clientMethodAPIToRule.get(clientId+Constants.KEY_DELIMITER+RuleType.API+Constants.KEY_DELIMITER+endPoint));	
        }
        return rules;
	}
	
	public abstract boolean processLimitsCounters(String clientId,String requestType ,String endPoint);
	
	

}
