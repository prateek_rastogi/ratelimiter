package com.ninja.limiter.service;

import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ninja.core.limiter.request.ThrottleRule;
import com.ninja.limiter.core.LimiterCore;

@Service(value="MemprySyncRateLimiter")
public class MemorySycnRateLimiterService extends AbstractRateLimiter{
	private static final Logger logger = LoggerFactory.getLogger(MemorySycnRateLimiterService.class);

	private final String KEY_SEPERATOR = ".";

	@Autowired
	LimiterCore limiterCore;

	@Autowired
	ServerInfoService serverService;

	@Override
	public void loadClientConfig(){
		super.loadClientConfig();
	}
	@Scheduled(initialDelay = 1500, fixedRate = 30000)
	public void loadConfigToMemory(){
		logger.info("Loading Client Config to Memory ..");
		int serverCount = serverService.getServerCount();
		for(Map.Entry<String, Set<ThrottleRule>> entry : clientMethodAPIToRule.entrySet()){
			Set<ThrottleRule> throttleRule = entry.getValue();			
			for(ThrottleRule rule : throttleRule){
				StringBuilder key = new StringBuilder();
				key.append(rule.getCliendId()+KEY_SEPERATOR);
				key.append(rule.getRule().getType()+KEY_SEPERATOR);
				key.append(rule.getRule().getRuleKey()+KEY_SEPERATOR);
				key.append(rule.getLimit().getTimeUnit());
				limiterCore.createUpdateKey(key.toString(), getCalculatedThreshold(rule.getLimit().getLimit().intValue(),serverCount), rule.getLimit().getExpiry().longValue());					
			}			
		}
		logger.info("Client Config Loaded To Memory SUCCESSFULL ..");
	}

	//TODO Logic can be extended based on priority of servers who would take extra hit in case of Mod is not not 0
	private int getCalculatedThreshold(int threshold,int serverCount){
		Double modCount = threshold % serverCount == 0 ? threshold / serverCount : (double)threshold / serverCount; 
		return (int) Math.ceil(modCount);
	}
	@Override
	public boolean processLimitsCounters(String clientId,String requestType ,String endPoint){
		Boolean retVal = true;
		Set<ThrottleRule> rules = fetchMatchingRules(clientId,requestType,endPoint);
		if(ObjectUtils.isEmpty(rules)){
			return retVal;
		}
		for(ThrottleRule throttleRule : rules){
			String key = clientId+KEY_SEPERATOR+throttleRule.getRule().getType()+KEY_SEPERATOR+throttleRule.getRule().getRuleKey()+KEY_SEPERATOR+throttleRule.getLimit().getTimeUnit();
			retVal = limiterCore.incr(key);
			if(!retVal){
				return retVal;
			}
		}
		return retVal;
	}
}
