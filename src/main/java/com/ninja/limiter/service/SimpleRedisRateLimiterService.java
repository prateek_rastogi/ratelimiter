package com.ninja.limiter.service;

import java.util.Arrays;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.google.gson.Gson;
import com.ninja.core.limiter.request.ThrottleRule;

@Service
public class SimpleRedisRateLimiterService extends AbstractRateLimiter{
	private static final Logger logger = LoggerFactory.getLogger(SimpleRedisRateLimiterService.class);

	@Autowired
	private RedisTemplate<String, Integer> template;
	
	
	@Override
	public boolean processLimitsCounters(String clientId,String requestType ,String endPoint){
		Boolean retVal = true;
		Set<ThrottleRule> rules = fetchMatchingRules(clientId,requestType,endPoint);
		if(ObjectUtils.isEmpty(rules)){
			return retVal;
		}
		DefaultRedisScript<Boolean> redisScript = new DefaultRedisScript<Boolean>();
		redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("rate_limit.lua")));
		redisScript.setResultType(Boolean.class);
		Gson gson = new Gson();
		String rulesJson = gson.toJson(rules);
		try{
			logger.debug(rulesJson.toString());
			retVal =  template.execute(redisScript , Arrays.asList(),clientId,rulesJson);
		}catch(Exception e){
			retVal = false;
			logger.error(e.getMessage());
		}
		return retVal;
	}
}
