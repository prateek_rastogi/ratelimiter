package com.ninja.limiter.service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.ninja.limiter.core.ServerInfo;
import com.ninja.limiter.core.repository.ServerRepository;



@Service
@Scope(value="singleton")
public class ServerInfoService {
	private static final Logger logger = LoggerFactory.getLogger(ServerInfoService.class);

	private AtomicBoolean isRunning = new AtomicBoolean(false);
	
	private int serverCount = 0;

	@Autowired
	ServerRepository serverRepository;

	public Integer getServerCount(){
		return serverCount;
	}

	@Scheduled(initialDelay = 500, fixedRate = 30000)
	public void registerLoadServerInfo(){
		logger.info("Loading Server Info..");	
		try {
			registerServerInfo();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fetchServerCount();
		logger.info("Server Info Loaded Successfully..");
	}
	
	public void registerServerInfo() throws Exception{
		if(isRunning.get()){
			return;
		}
		isRunning.set(true);
		String serverIdentifier = ""; 
		try {
			serverIdentifier = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			serverIdentifier = "";
			throw e;
		}
		ServerInfo server = serverRepository.findByServerIdentifier(serverIdentifier);
		if(null == server){
			server = new ServerInfo();
			server.setServerIdentifier(serverIdentifier);
			server.setPriority(10); //Extension of Logic
			server.setPriorityBucket(100); 
			server.setCreated(Calendar.getInstance().getTime());        	 	 
		}
		server.setUpdated(Calendar.getInstance().getTime());
		serverRepository.save(server);
		isRunning.set(false);
	}
	public void fetchServerCount(){		
		Date currDate = new Date();		
		Date backDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(backDate);
		c.add(Calendar.SECOND, -600);	
		serverCount =  serverRepository.fetchCountByDateRange(c.getTime(),currDate);     
	}

}
