
local clientId = ARGV[1];
local rules = cjson.decode(ARGV[2]);
--local rules = ARGV[2];
for i, rule in ipairs(rules) do
	local key = clientId .. '.' .. rule.rule.type .. '.' .. rule.rule.ruleKey .. '.' .. rule.limit.timeUnit;
  local current = redis.call("incr",key);	
 if tonumber(current) == 1 then
 	redis.call("expire",key,tonumber(rule.limit.expiry));
 end
 if tonumber(current) > rule.limit.limit then
 	return false;
 end
end
return true;

